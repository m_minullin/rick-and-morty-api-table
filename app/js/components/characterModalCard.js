import localStorageApi from "../api/localStorage-api.js";
import { getEpisode } from "../api/rick-and-morty-api.js";

const createCharacterCard = (id) => {
  const character = localStorageApi.getOne("data", id);

  const card = document.createElement("div");
  card.className = "character-card";

  const imgWrapper = document.createElement("div");
  imgWrapper.className = "character-card__img-wrapper";

  const img = document.createElement("img");
  img.className = "character-card__image";
  img.src = character.image;

  const wrapper = document.createElement("div");
  wrapper.className = "character-card__info-card-wrapper";

  const characterInfoWrapper = document.createElement("ul");
  characterInfoWrapper.className = "character-card__info-wrapper";

  const name = document.createElement("li");
  name.className = "character-card__info";
  name.append(`Name : ${character.name}`);

  const status = document.createElement("li");
  status.className = "character-card__info";
  status.append(`Status : ${character.status}`);

  const originLocation = document.createElement("li");
  originLocation.className = "character-card__info";
  originLocation.append(`Origin : ${character.origin.name}`);

  const lastLocation = document.createElement("li");
  lastLocation.className = "character-card__info";
  lastLocation.append(`Last location : ${character.location.name}`);

  const episodeList = document.createElement("ul");
  episodeList.className = "character-card__episode-list";

  const episodeTitle = document.createElement("h2");
  episodeTitle.className = "character-card__episode-title";
  episodeTitle.append("Episodes :");

  const arrEpisodes = getEpisodeList(character.episode);
  if (character.episode[0] === "unknown") {
    const liHTML = document.createElement("li");
    liHTML.className = "character-card__episode-item";
    liHTML.append("unknown");
    episodeList.append(liHTML);
  } else {
    getEpisode(arrEpisodes).then((r) => {
      if (Array.isArray(r)) {
        r.forEach((item) => {
          const liHTML = document.createElement("li");
          liHTML.className = "character-card__episode-item";
          liHTML.append(item.name);
          episodeList.append(liHTML);
        });
      } else {
        const liHTML = document.createElement("li");
        liHTML.className = "character-card__episode-item";
        liHTML.append(r.name);
        episodeList.append(liHTML);
      }
    });
  }

  imgWrapper.append(img);

  characterInfoWrapper.append(name);
  characterInfoWrapper.append(status);
  characterInfoWrapper.append(originLocation);
  characterInfoWrapper.append(lastLocation);

  wrapper.append(characterInfoWrapper);
  wrapper.append(episodeTitle);
  wrapper.append(episodeList);

  card.append(imgWrapper);
  card.append(wrapper);

  return card;
};

const getEpisodeList = (arr) => {
  return arr.map((string) => {
    return parseInt(string.replace(/[^\d]/g, ""));
  });
};

export default createCharacterCard;
