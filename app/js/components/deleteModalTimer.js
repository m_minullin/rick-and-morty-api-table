import modal from "./modal.js";

const createModalTimer = () => {
  const wrapper = document.createElement("div");
  wrapper.className = "delete-timer";

  const icon = document.createElement("div");
  icon.className = "delete-timer__icon";

  const title = document.createElement("h2");
  title.className = "delete-timer__title";
  title.append("Are you sure?");

  const text = document.createElement("p");
  text.className = "delete-timer__text";
  text.append("You will not be able to restore this character !");

  const timer = document.createElement("div");
  timer.className = "delete-timer__timer";
  timer.append("30");

  const buttonWrapper = document.createElement("div");
  buttonWrapper.className = "delete-timer__button-wrapper";

  const cancelButton = document.createElement("button");
  cancelButton.className = "delete-timer__cancel-button";
  cancelButton.append("Cancel");

  const deleteButton = document.createElement("button");
  deleteButton.className = "delete-timer__delete-button";
  deleteButton.append("Yes, delete it!");

  buttonWrapper.append(cancelButton);
  buttonWrapper.append(deleteButton);

  wrapper.append(icon);
  wrapper.append(title);
  wrapper.append(text);
  wrapper.append(timer);
  wrapper.append(buttonWrapper);

  return wrapper;
};

const startTimer = (ref) => {
  let sec = 30;

  const changeSec = (value) => {
    ref.innerHTML = "";
    ref.append(value);
  };

  const timer = setInterval(() => {
    sec -= 1;
    changeSec(sec);

    if (sec <= 0) {
      clearInterval(timer);
      modal.closeModal();
    }
  }, 1000);
};

export { createModalTimer, startTimer };
