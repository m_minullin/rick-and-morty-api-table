import refs from "../utils/refs.js";

const createTable = (arr) => {
  arr.forEach(({ id, name, status, image }) => {
    const itemHTML = document.createElement("li");
    itemHTML.className = "content-table__item";

    const infoWrapper = document.createElement("div");
    infoWrapper.className = "content-table__info-wrapper";
    infoWrapper.id = id;

    const imageHTML = document.createElement("img");
    imageHTML.className = "content-table__image";
    imageHTML.src = image;
    imageHTML.alt = name;

    const nameHTML = document.createElement("h2");
    nameHTML.className = "content-table__name";

    const statusHTML = document.createElement("p");
    statusHTML.className = "content-table__status";

    const buttonsWrapper = document.createElement("div");
    buttonsWrapper.className = "content-table__button-wrapper";

    const deleteButton = document.createElement("button");
    deleteButton.className = "content-table__delete-button";
    deleteButton.dataset.id = id;
    deleteButton.append("delete");

    const editButton = document.createElement("button");
    editButton.className = "content-table__edit-button";
    editButton.dataset.id = id;
    editButton.append("edit");

    buttonsWrapper.append(deleteButton);
    buttonsWrapper.append(editButton);

    nameHTML.append(name);
    statusHTML.append(status);

    infoWrapper.append(imageHTML);
    infoWrapper.append(nameHTML);
    infoWrapper.append(statusHTML);

    itemHTML.append(infoWrapper);
    itemHTML.append(buttonsWrapper);

    refs.rootRef.append(itemHTML);
  });
};

export default createTable;
