import modal from "./modal.js";
import localStorageApi from "../api/localStorage-api.js";

const editForm = {
  createEditForm(id) {
    const item = localStorageApi.getOne("data", id);

    const formHTML = document.createElement("div");
    formHTML.className = "edit-form";

    const nameInputHTML = document.createElement("input");
    nameInputHTML.className = "edit-form__input";
    nameInputHTML.name = "name";
    nameInputHTML.id = "edit-name";
    nameInputHTML.placeholder = item.name;

    const statusInputHTML = document.createElement("input");
    statusInputHTML.className = "edit-form__input";
    statusInputHTML.name = "status";
    statusInputHTML.placeholder = item.status;
    statusInputHTML.id = "edit-status";

    const originInputHTML = document.createElement("input");
    originInputHTML.className = "edit-form__input";
    originInputHTML.name = "origin";
    originInputHTML.id = "edit-origin";
    originInputHTML.placeholder = item.origin.name;

    const lastLocationInputHTML = document.createElement("input");
    lastLocationInputHTML.className = "edit-form__input";
    lastLocationInputHTML.name = "last location";
    lastLocationInputHTML.placeholder = item.location.name;
    lastLocationInputHTML.id = "edit-last-location";

    const saveButtonHTML = document.createElement("button");
    saveButtonHTML.className = "edit-form__button-save";
    saveButtonHTML.id = "save-button";

    saveButtonHTML.append("save");

    formHTML.append(nameInputHTML);
    formHTML.append(statusInputHTML);
    formHTML.append(originInputHTML);
    formHTML.append(lastLocationInputHTML);
    formHTML.append(saveButtonHTML);

    return formHTML;
  },

  handleSubmit({ id, name, status, origin, location }) {
    if (name === "" && status === "" && origin === "" && location === "")
      return;
    else {
      const character = localStorageApi.getOne("data", id);

      const newCharacter = {
        ...character,
        name: name !== "" ? name : character.name,
        status: status !== "" ? status : character.status,
        origin: origin !== "" ? origin : character.origin,
        location: location !== "" ? location : character.location,
      };

      localStorageApi.put("data", newCharacter);
      modal.closeModal();
    }
  },
};

export default editForm;
