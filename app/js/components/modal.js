import refs from "../utils/refs.js";

const modal = {
  openModal() {
    refs.modalRef.classList.add("active");

    refs.modalButtonRef.onclick = (e) => {
      this.closeModal(e);
    };
  },

  closeModal() {
    refs.modalRef.classList.remove("active");

    setTimeout(() => {
      refs.modalContentRef.innerHTML = "";
    }, 500);
  },
};

export default modal;
