import { v4 } from "uuid";
import modal from "./modal.js";
import localStorageApi from "../api/localStorage-api.js";
const baseImg =
  "https://img.utdstc.com/icon/960/a82/960a826be969bbaa37e3e54cb6511e8cc0c4488627d011eecd6b9000301dc2fb:200";

const createForm = {
  createForm() {
    const formHTML = document.createElement("div");
    formHTML.className = "create-form";

    const nameInputHTML = document.createElement("input");
    nameInputHTML.className = "create-form__input";
    nameInputHTML.name = "name";
    nameInputHTML.placeholder = "name";
    nameInputHTML.id = "create-name";

    const statusInputHTML = document.createElement("input");
    statusInputHTML.className = "create-form__input";
    statusInputHTML.name = "status";
    statusInputHTML.placeholder = "status";
    statusInputHTML.id = "create-status";

    const originInputHTML = document.createElement("input");
    originInputHTML.className = "create-form__input";
    originInputHTML.name = "origin";
    originInputHTML.placeholder = "origin";
    originInputHTML.id = "create-origin";

    const locationInputHTML = document.createElement("input");
    locationInputHTML.className = "create-form__input";
    locationInputHTML.name = "location";
    locationInputHTML.placeholder = "location";
    locationInputHTML.id = "create-location";

    const createButtonHTML = document.createElement("button");
    createButtonHTML.className = "create-form__button";
    createButtonHTML.id = "create";

    createButtonHTML.append("create");

    formHTML.append(nameInputHTML);
    formHTML.append(statusInputHTML);
    formHTML.append(originInputHTML);
    formHTML.append(locationInputHTML);
    formHTML.append(createButtonHTML);

    return formHTML;
  },

  handleClick({ name, status, origin, location }) {
    if (name === "" && status === "" && origin === "" && location === "")
      return;
    else {
      const newCharacter = {
        id: v4(),
        name: !name ? "unknown" : name,
        status: !status ? "unknown" : status,
        origin: { name: !origin ? "unknown" : origin },
        location: { name: !location ? "unknown" : location },
        episode: ["unknown"],
        image: baseImg,
      };
      localStorageApi.add("data", newCharacter);
      modal.closeModal();
    }
  },
};

export default createForm;
