import refs from "./utils/refs.js";
import modal from "./components/modal.js";
import editForm from "./components/edit-fom.js";
import createForm from "./components/createForm.js";
import createTable from "./components/table.js";
import createCharacterCard from "./components/characterModalCard.js";
import { getData } from "./api/rick-and-morty-api.js";
import localStorageApi from "./api/localStorage-api.js";
import { createModalTimer, startTimer } from "./components/deleteModalTimer.js";

async function app() {
  const data = await localStorageApi.get("data");

  if (data) createTable(data);
  else {
    const arr = await getData();
    arr.json().then(({ results }) => {
      localStorageApi.post("data", results);
      const data = localStorageApi.get("data");
      createTable(data);
    });
  }

  refs.createButtonRef.onclick = () => {
    const form = createForm.createForm();
    refs.modalContentRef.append(form);
    modal.openModal();

    const nameInput = document.getElementById("create-name");
    const statusInput = document.getElementById("create-status");
    const originInput = document.getElementById("create-origin");
    const locationInput = document.getElementById("create-location");
    const createModalButton = document.getElementById("create");

    createModalButton.onclick = () => {
      const newData = {
        name: nameInput.value,
        status: statusInput.value,
        origin: originInput.value,
        location: locationInput.value,
      };
      createForm.handleClick(newData);

      const newTable = localStorageApi.get("data");
      refs.rootRef.innerHTML = "";
      createTable(newTable);
    };
  };

  refs.rootRef.onclick = (e) => {
    const { className, id } = e.target;

    if (className === "content-table__info-wrapper") {
      const characterCard = createCharacterCard(id);
      refs.modalContentRef.append(characterCard);
      modal.openModal();
    } else if (className === "content-table__delete-button") {
      const { id } = e.target.dataset;

      const timerCard = createModalTimer();
      refs.modalContentRef.append(timerCard);
      const timer = document.querySelector(".delete-timer__timer");
      const cancelButton = document.querySelector(
        ".delete-timer__cancel-button"
      );

      const deleteButton = document.querySelector(
        ".delete-timer__delete-button"
      );
      startTimer(timer);
      modal.openModal();

      cancelButton.onclick = (e) => {
        modal.closeModal();
      };

      deleteButton.onclick = () => {
        modal.closeModal();
        localStorageApi.delete("data", id);
        const newTable = localStorageApi.get("data");
        refs.rootRef.innerHTML = "";
        createTable(newTable);
      };
    } else if (className === "content-table__edit-button") {
      const { id } = e.target.dataset;

      const form = editForm.createEditForm(id);
      refs.modalContentRef.append(form);
      modal.openModal();
      const nameInput = document.getElementById("edit-name");
      const statusInput = document.getElementById("edit-status");
      const originInput = document.getElementById("edit-origin");
      const locationInput = document.getElementById("edit-last-location");
      const saveButton = document.getElementById("save-button");

      saveButton.onclick = () => {
        const data = {
          id: id,
          name: nameInput.value,
          status: statusInput.value,
          origin: originInput.value,
          location: locationInput.value,
        };

        editForm.handleSubmit(data);
        const newTable = localStorageApi.get("data");
        refs.rootRef.innerHTML = "";
        createTable(newTable);
      };
    }
  };
}

document.addEventListener("DOMContentLoaded", app);
