import MyPromise from "../utils/my-promise.js";

const baseUrl = "https://rickandmortyapi.com/api";

const getData = () => {
  return new MyPromise((res, rej) => {
    const request = fetch(`${baseUrl}/character`, {
      mode: "cors",
      headers: {
        "Access-Control-Allow-Origin": "http://localhost:3000/",
      },
    });

    if (request) {
      setTimeout(() => res(request), 250);
    } else {
      rej(new Error("Network error"));
    }
  });
};

async function getEpisode(arr) {
  const data = await fetch(`${baseUrl}/episode/${arr}`, {
    mode: "cors",
    headers: {
      "Access-Control-Allow-Origin": "http://localhost:3000/",
    },
  });

  const parseData = data.json();
  return parseData;
}

export { getData, getEpisode };
