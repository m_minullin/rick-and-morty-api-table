const localStorageApi = {
  get(key) {
    const storageData = localStorage.getItem(key);
    return JSON.parse(storageData);
  },

  getOne(key, id) {
    const storageData = localStorage.getItem(key);
    const arr = JSON.parse(storageData);

    const item = arr.filter((i) => i.id.toString() === id);
    return item[0];
  },

  add(key, item) {
    const storageData = localStorage.getItem(key);
    const arr = JSON.parse(storageData);
    arr.push(item);
    this.post("data", arr);
  },

  post(key, arr) {
    const storageData = JSON.stringify(arr);
    localStorage.setItem(key, storageData);
  },

  put(key, item) {
    const storageData = localStorage.getItem(key);
    const arr = JSON.parse(storageData);
    const newArr = arr.filter((i) => i.id !== item.id);
    newArr.push(item);
    this.post("data", newArr);
  },

  delete(key, id) {
    const storageData = localStorage.getItem(key);
    const arr = JSON.parse(storageData);

    const data = arr.filter((i) => i.id.toString() !== id);

    this.post("data", data);
  },
};

export default localStorageApi;
