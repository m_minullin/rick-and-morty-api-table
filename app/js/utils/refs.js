export default {
  rootRef: document.querySelector("#root"),
  modalRef: document.querySelector("#modal"),
  modalContentRef: document.querySelector(".modal__content"),
  modalButtonRef: document.querySelector("#modal-button"),
  createButtonRef: document.querySelector(".create-button"),
};
